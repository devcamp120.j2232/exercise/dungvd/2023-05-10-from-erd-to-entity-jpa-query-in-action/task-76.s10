package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.service.ProductLineService;

@RestController
@CrossOrigin
public class ProductLineController {
    @Autowired
    ProductLineService productLineService;



    // API lấy ra toàn bộ product line
    @GetMapping("/productlines")
    public ResponseEntity<Object> getAllProductLine() {
        try {
            List<ProductLine> allProductLines = productLineService.getAllProductLines();
            return new ResponseEntity<>(allProductLines, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 product line
    @PostMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> createNewProductLine(@PathVariable("id") Long id, @RequestBody ProductLine pProductLine) {
        try {
            ProductLine createdProductLine = productLineService.createProductLine(pProductLine);
            return new ResponseEntity<>(createdProductLine, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật 1 product line
    @PutMapping("/productlines/{productlineId}") 
    public ResponseEntity<ProductLine> updateProductLine(@PathVariable("productlineId") Long productlineId, 
                                                        @RequestBody ProductLine pProductLine) {
        try {
            ProductLine updatedProductLine = productLineService.updateProductLine(productlineId, pProductLine);
            return new ResponseEntity<>(updatedProductLine, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 product
    @DeleteMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> deleteProductLine(@PathVariable("id") Long id) {
        try {
            productLineService.deleteProductLine(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
}
