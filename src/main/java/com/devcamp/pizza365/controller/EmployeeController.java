package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.service.EmployeeService;

@RestController
@CrossOrigin
public class EmployeeController {
    @Autowired
    EmployeeService iEmployeeService;

    // API lấy ra toàn bộ employee
    @GetMapping("/employees")
    public ResponseEntity<Object> getAllEmployees() {
        try {
            List<Employee> allEmployees = iEmployeeService.getAllEmployees();
            return new ResponseEntity<>(allEmployees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 employee
    @PostMapping("/employees/{id}")
    public ResponseEntity<Employee> createEmployee(@PathVariable("id") Long id, @RequestBody Employee pEmployee) {
        try {
            Employee createdEmployee = iEmployeeService.createEmployee(pEmployee);
            return new ResponseEntity<>(createdEmployee, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật 1 product line
    @PutMapping("/employees/{id}") 
    public ResponseEntity<Employee> updateEmployees(@PathVariable("id") Long id, 
                                                    @RequestBody Employee pEmployee) {
        try {
            Employee updatedEmployee = iEmployeeService.updateEmployee(id,pEmployee);
            return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 product
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") Long id) {
        try {
            iEmployeeService.deleteEmployee(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
}
