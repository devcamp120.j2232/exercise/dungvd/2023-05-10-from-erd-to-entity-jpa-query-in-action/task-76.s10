package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Long>{
    @Query(value = "SELECT * FROM employees WHERE last_name LIKE :lastName%", nativeQuery = true)
    List<Employee> findByLastName(@Param("lastName") String lastName);

    @Query(value = "SELECT * FROM employees WHERE first_name LIKE :firstName%", nativeQuery = true)
    List<Employee> findByFirstName(@Param("firstName") String firstName);

    @Query(value = "SELECT * FROM employees WHERE job_title LIKE :jobTitle%", nativeQuery = true)
    List<Employee> findByJobTitle(@Param("jobTitle") String jobTitle);

    @Transactional
    @Modifying
    @Query(value = "UPDATE employees SET report_to :=reportTo% WHERE report_to = 0")
    String updateReportTo(@Param("reportTo") String reportTo);
}
