package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Product;

public interface IProductRepository extends JpaRepository<Product, Long>{
    @Query(value = "SELECT * FROM products WHERE product_name LIKE :productName%", nativeQuery = true)
    List<Product> findByProductName(@Param("productName") String productName);

    @Query(value = "SELECT * FROM products WHERE product_code LIKE :productCode%", nativeQuery = true)
    List<Product> findByProductCode(@Param("productCode") String productCode);
}
