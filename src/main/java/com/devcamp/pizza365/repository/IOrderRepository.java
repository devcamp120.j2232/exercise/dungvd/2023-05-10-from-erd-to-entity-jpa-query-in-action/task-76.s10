package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;
import javax.websocket.server.PathParam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
	COrder findByOrderCode(String orderId);

	@Query(value = "SELECT * FROM orders WHERE status LIKE :status%",nativeQuery= true)
	List<Order> findByStatus(@PathParam("status") String status);

	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET comments :=comment% WHERE comments IS null", nativeQuery = true)
	String updateComments(@PathParam("comment") String comment);
}
