package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository<ProductLine, Long>{
    @Query(value = "SELECT * FROM product_lines WHERE product_line LIKE :productLine%", nativeQuery = true)
    List<ProductLine> findByProductLine(@Param("productLine") String productLine);
}
