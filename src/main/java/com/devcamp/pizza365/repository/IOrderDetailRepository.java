package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail, Long>{
    @Query(value = "SELECT * FROM order_details WHERE order_id LIKE :orderId%", nativeQuery = true)
    List<OrderDetail> findByOrderId(@Param("id") String orderId);

    @Query(value = "SELECT * FROM order_details WHERE product_id LIKE :productId%", nativeQuery = true)
    List<OrderDetail> findByProductId(@Param("id") Long producId);
}
