package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Long>{
    @Query(value = "SELECT * FROM payments WHERE customer_id LIKE :customerId%", nativeQuery = true)
    List<Payment> findByCustomerIdLike(@Param("customerId") Long customerId);

    @Query(value = "SELECT * FROM payments WHERE check_number LIKE :checkNumber%", nativeQuery = true)
    Payment findByCheckNumberLike(@Param("checkNumber") String checkNumber);

    @Transactional
    @Modifying
    @Query(value = "UPDATE payments SET check_number = :checkNumber WHERE check_number IS null", nativeQuery = true)
    String updateCheckNumber(@Param("checkNumber") String checkNumber);
}
