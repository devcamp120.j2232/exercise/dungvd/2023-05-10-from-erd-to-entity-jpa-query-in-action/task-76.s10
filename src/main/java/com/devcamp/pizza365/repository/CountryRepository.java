package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCountry;

import java.util.List;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
	
	@Query(value = "FROM #{#entityName} WHERE countryName like %?1%")
	List<CCountry> findCountryByCountryNameLike(String countryname, Pageable pageable);
	
	@Query(value = "FROM CCountry WHERE country_name like %?1%")
	List<CCountry> findCountryByCountryNameLike1(String countryname);

	@Query(value = "FROM CCountry WHERE countryName like %?1%")
	List<CCountry> findCountryByCountryNameLike2(String countryname);

	@Query(value = "SELECT * FROM p_country WHERE country_name LIKE :countryName%", nativeQuery = true)
	List<CCountry> findCountryByCountryNameLike3(@Param("countryName") String countryName);

	@Query(value = "SELECT * FROM p_country WHERE country_name like %:name1%", nativeQuery = true)
	List<CCountry> timCountryByCountryNameLike(@Param("name1") String countryname);
	
	@Query(value = "SELECT * FROM p_country WHERE country_name like :name1 ORDER BY country_name DESC", nativeQuery = true)
	List<CCountry> findCountryByCountryNameDesc(@Param("name1") String name1);
}
