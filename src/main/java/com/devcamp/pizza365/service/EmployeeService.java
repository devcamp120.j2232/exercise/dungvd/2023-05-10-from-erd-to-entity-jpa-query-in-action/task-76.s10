package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    IEmployeeRepository iEmployeeRepository;

    // Hàm lấy ra danh sách toàn bộ employeee
    public List<Employee> getAllEmployees() {
        List<Employee> listEmployees = new ArrayList<>();
        iEmployeeRepository.findAll().forEach(listEmployees::add);
        return listEmployees;
    }

    // Hàm tạo mới 1 employee
    public Employee createEmployee(Employee pEmployee) {

        Employee employeeCreated = iEmployeeRepository.save(pEmployee);
        return employeeCreated;
    }

    // Hàm update 1 employee
    public Employee updateEmployee(Long employeeId, Employee pEmployee) {
    
        Employee employee = iEmployeeRepository.findById(employeeId).get();
        employee.setEmail(pEmployee.getEmail());
        employee.setExtension(pEmployee.getExtension());
        employee.setFirstName(pEmployee.getFirstName());
        employee.setLastName(pEmployee.getLastName());
        employee.setJobTitle(pEmployee.getJobTitle());
        employee.setOfficeCode(pEmployee.getOfficeCode());
        employee.setReportTo(pEmployee.getReportTo());


        Employee employeeUpdated = iEmployeeRepository.save(employee);
        return employeeUpdated;
    }

    // Hàm xóa 1 product
    public void deleteEmployee(Long employeeId) {
        iEmployeeRepository.deleteById(employeeId);
    }
}
