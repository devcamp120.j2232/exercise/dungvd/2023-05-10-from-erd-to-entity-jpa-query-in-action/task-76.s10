package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;

@Service
public class ProductLineService {
    @Autowired
    IProductLineRepository iProductLineRepository;

    // Hàm lấy ra danh sách toàn bộ product line
    public List<ProductLine> getAllProductLines() {
        List<ProductLine> listProductLine = new ArrayList<>();
        iProductLineRepository.findAll().forEach(listProductLine::add);
        return listProductLine;
    }

    // Hàm tạo mới 1 product Line
    public ProductLine createProductLine(ProductLine pProductLine) {
        ProductLine productLineCreated = iProductLineRepository.save(pProductLine);
        return productLineCreated;
    }

    // Hàm update 1 product line
    public ProductLine updateProductLine(Long productLineId, ProductLine pProductLine) {
        ProductLine productLine = iProductLineRepository.findById(productLineId).get();
        productLine.setDescription(pProductLine.getDescription());
        productLine.setProductLine(pProductLine.getProductLine());

        ProductLine productLineUpdated = iProductLineRepository.save(productLine);
        return productLineUpdated;
    }

    // Hàm xóa 1 product
    public void deleteProductLine(Long productLineId) {
        iProductLineRepository.deleteById(productLineId);
    }
}
