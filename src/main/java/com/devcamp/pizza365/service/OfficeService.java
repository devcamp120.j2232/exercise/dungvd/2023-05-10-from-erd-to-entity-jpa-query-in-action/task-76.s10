package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

@Service
public class OfficeService {
    @Autowired
    IOfficeRepository iOfficeRepository;

    // Hàm lấy ra danh sách toàn bộ office
    public List<Office> getAllOffices() {
        List<Office> listOffices = new ArrayList<>();
        iOfficeRepository.findAll().forEach(listOffices::add);
        return listOffices;
    }

    // Hàm tạo mới 1 office
    public Office createOffice(Office pOffice) {

        Office officeCreated = iOfficeRepository.save(pOffice);
        return officeCreated;
    }

    // Hàm update 1 office
    public Office updateOffice(Long officeId, Office pOffice) {
    
        Office office = iOfficeRepository.findById(officeId).get();
        office.setAddressLine(pOffice.getAddressLine());
        office.setCity(pOffice.getCity());
        office.setCountry(pOffice.getCountry());
        office.setPhone(pOffice.getPhone());
        office.setState(pOffice.getState());
        office.setTerritory(pOffice.getTerritory());
        Office officeUpdated = iOfficeRepository.save(office);
        return officeUpdated;
    }

    // Hàm xóa 1 office
    public void deleteOffice(Long officeId) {
        iOfficeRepository.deleteById(officeId);
    }
}
